package me.neutze.licensetest;

import com.google.android.vending.licensing.AESObfuscator;
import com.google.android.vending.licensing.LicenseChecker;
import com.google.android.vending.licensing.LicenseCheckerCallback;
import com.google.android.vending.licensing.ServerManagedPolicy;

import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import me.neutze.lvltest.R;


public class MainActivity extends AppCompatActivity {

    private static final String BASE64_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAo"
            + "/iY2nt0OvE0DsOj0TBg+ue1s7NDjyzL+XXV8bz4EaWWIayFHFaMjJ6yX/XsL1uLpE6WqvVFrFwjcnGx4uP4"
            + "YHpE3eTKMkV4ubMdUCEKkGu4t4NxnS46rOdEn8kouLx2katPF2HN5padsDgDj+51p0CEDewG3A2QST+EQK/"
            + "fhds0TTP/nOgv8Qm8P7l0iayf6KbzY8E8oI422Ep7xqz1uFxmQn0VXETZnjcTh6SaA6k67lxk+vVHwDB44z"
            + "gFp9HBGRAZC5gQEji483AyQCKs5z9WaFo2StEDiyeI11v+LT/bTEVhZZAGQHdR8nuJxh1Zfw6anuHcTtTfs"
            + "uNzU7wn/wIDAQAB";
    private static final byte[] SALT = new byte[]{
            -20, 30, 50, -70, 33, -100, 32, -90, -88, 104,
            12, -10, 72, -34, 115, 21, 62, 35, -12, 97};
    private Handler mHandler;
    private TextView mStatusText;
    private Button mCheckLicenseButton;
    private Toolbar mToolbar;
    private MyLicenseCheckerCallback mLicenseCheckerCallback;
    private LicenseChecker mChecker;

    @Override
    public void onCreate(final Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_main);

        mToolbar = (Toolbar) findViewById(R.id.toolbar_basic);
        mStatusText = (TextView) findViewById(R.id.text);
        mCheckLicenseButton = (Button) findViewById(R.id.button);

        initToolbar();

        final String mAndroidId = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        final AESObfuscator mObsfuscator = new AESObfuscator(SALT, getPackageName(), mAndroidId);
        final ServerManagedPolicy serverPolicy = new ServerManagedPolicy(this, mObsfuscator);
        mLicenseCheckerCallback = new MyLicenseCheckerCallback();
        mChecker = new LicenseChecker(this, serverPolicy, BASE64_PUBLIC_KEY);

        mCheckLicenseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHandler = new Handler();
                doCheck();
            }
        });
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
    }

    private void doCheck() {
        mCheckLicenseButton.setEnabled(false);
        setProgressBarIndeterminateVisibility(true);
        mStatusText.setText(R.string.checking_license);
        mChecker.checkAccess(mLicenseCheckerCallback);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mChecker.onDestroy();
    }

    private void displayResult(final String result) {
        mHandler.post(new Runnable() {
            public void run() {
                mStatusText.setText(result);
                setProgressBarIndeterminateVisibility(false);
                mCheckLicenseButton.setEnabled(true);
            }
        });
    }

    private class MyLicenseCheckerCallback implements LicenseCheckerCallback {

        @Override
        public void allow(final int reason) {
            if (isFinishing()) {
                return;
            }

            displayResult(getApplicationContext().getResources().getString(R.string.allow));
            Log.e("JOHANNES", getApplicationContext().getResources().getString(R.string.allow));
        }

        @Override
        public void dontAllow(final int reason) {
            if (isFinishing()) {
                return;
            }
            displayResult(getApplicationContext().getResources().getString(R.string.dont_allow));
            Log.e("JOHANNES", getApplicationContext().getResources().getString(R.string.dont_allow));
        }

        @Override
        public void applicationError(final int errorCode) {
            displayResult(getApplicationContext().getResources().getString(R.string.error));
            Log.e("JOHANNES", getApplicationContext().getResources().getString(R.string.error));
        }
    }
}
